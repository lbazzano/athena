# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

TauJetsCPContent = [
"AntiKt4LCTopoJets",
"InDetTrackParticles",
"InDetTrackParticlesAux.phi.vertexLink.theta.qOverP.truthParticleLink.truthMatchProbability",
"TauJets",
"TauJetsAux.pt.eta.phi.m.tauTrackLinks.jetLink.vertexLink.secondaryVertexLink.charge.isTauFlags.BDTJetScore.BDTEleScore.ptFinalCalib.etaFinalCalib.phiFinalCalib.mFinalCalib.electronLink.EleMatchLikelihoodScore.pt_combined.eta_combined.phi_combined.m_combined.BDTJetScoreSigTrans.BDTEleScoreSigTrans.PanTau_DecayMode.RNNJetScore.RNNJetScoreSigTrans.seedJetWidth.seedJetJvt.seedTrackWidthPt1000.seedTrackWidthPt500.truthParticleLink.truthJetLink",
"TauTracks",
"TauTracksAux.pt.eta.phi.flagSet.trackLinks",
"TruthTaus",
"TruthTausAux.pt_vis_neutral_pions.pt_vis_neutral_others.numCharged.classifierParticleType.classifierParticleOrigin.IsHadronicTau.numNeutralPion.numNeutral.numChargedPion.pt_vis.eta_vis.phi_vis.m_vis",
"TauSecondaryVertices",
"TauSecondaryVerticesAux.x.y.z.covariance.trackParticleLinks"
]
